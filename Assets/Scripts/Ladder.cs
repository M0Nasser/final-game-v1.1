﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    public float LadderForce = 100f;
    public bool IsCliming = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay(Collider other)
    {
        if(other.tag=="Player")
        {
            Debug.Log("entred ");
            IsCliming = true;
            other.GetComponent<Rigidbody>().useGravity = false;
            //other.GetComponent<Rigidbody>().isKinematic = false;
            
            if(Input.GetKeyDown(KeyCode.W))
                //other.transform.position+=Vector3.up * Time.deltaTime;
                other.transform.Translate(Vector3.up*Time.deltaTime);
                //other.GetComponent<Rigidbody>().AddForce(Vector3.up * LadderForce);
            else if (Input.GetKeyDown(KeyCode.S))
                other.transform.Translate(Vector3.down* Time.deltaTime);
        }
    }
    private void OnTriggerExit(Collider other)
    {

        IsCliming = false;
        other.transform.Translate(Vector3.forward * Time.deltaTime);
         other.GetComponent<Rigidbody>().useGravity = true;
         //other.GetComponent<Rigidbody>().isKinematic = true;
    }
}
