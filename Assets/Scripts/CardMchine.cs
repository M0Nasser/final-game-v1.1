﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;

public class CardMchine : MonoBehaviour
{
    public GameObject text;
    public PlayableDirector playableDirector;


    public GameObject SlotHolder;


    public int AllSlots;
    public int ActiveSlot;
    public GameObject[] slot;

    Renderer rend;
    Timer timer;

    // Start is called before the first frame update
    void Start()
    {
        rend = GetComponent<Renderer>();
        text.SetActive(false);

        timer = gameObject.AddComponent<Timer>();
        timer.Duration = 3;

        AllSlots = 2;
        slot = new GameObject[AllSlots];

        for (int i = 0; i < AllSlots; i++)
        {
            slot[i] = SlotHolder.transform.GetChild(i).gameObject;
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            text.SetActive(true);

            if (Input.GetKeyDown(KeyCode.E))
            {
                for (int i = 0; i < AllSlots - 1; i++)
                {
                    if (slot[i].GetComponent<Slot>().ID == 1 &&
                        slot[i + 1].GetComponent<Slot>().ID == 0 &&
                        slot[i + 1].GetComponent<Slot>().Amount >= 6)
                    {
                        slot[i].GetComponent<Slot>().DeleteSlot();
                        slot[i].GetComponent<Slot>().empty = true;

                        slot[i].GetComponent<Slot>().Item = default;
                        slot[i].GetComponent<Slot>().ID = default;
                        slot[i].GetComponent<Slot>().Type = default;
                        slot[i].GetComponent<Slot>().Description = default;
                        slot[i].GetComponent<Slot>().Icon = default;
                        slot[i].GetComponent<Slot>().Amount = default;

                        if (slot[i + 1].GetComponent<Slot>().Amount > 6)
                        {
                            slot[i + 1].GetComponent<Slot>().Amount -= 6;
                            slot[i + 1].GetComponent<Slot>().UpdateSlot();
                        }
                        else if (slot[i + 1].GetComponent<Slot>().Amount == 6)
                        {
                            slot[i+1].GetComponent<Slot>().DeleteSlot();
                            slot[i+1].GetComponent<Slot>().empty = true;


                            slot[i+1].GetComponent<Slot>().Item = default;
                            slot[i+1].GetComponent<Slot>().ID = default;
                            slot[i+1].GetComponent<Slot>().Type = default;
                            slot[i+1].GetComponent<Slot>().Description = default;
                            slot[i+1].GetComponent<Slot>().Icon = default;
                            slot[i+1].GetComponent<Slot>().Amount = default;
                        }
                        playableDirector.Play();
                        timer.Run();

                    }
                    else if (slot[i].GetComponent<Slot>().ID == 0 &&
                        slot[i].GetComponent<Slot>().Amount >= 6 &&
                        slot[i+1].GetComponent<Slot>().ID == 1)
                    {
                        if (slot[i ].GetComponent<Slot>().Amount == 6)
                        {
                            slot[i].GetComponent<Slot>().DeleteSlot();
                            slot[i].GetComponent<Slot>().empty = true;


                            slot[i].GetComponent<Slot>().Item = default;
                            slot[i].GetComponent<Slot>().ID = default;
                            slot[i].GetComponent<Slot>().Type = default;
                            slot[i].GetComponent<Slot>().Description = default;
                            slot[i].GetComponent<Slot>().Icon = default;
                            slot[i].GetComponent<Slot>().Amount = default;
                        }
                        else if (slot[i].GetComponent<Slot>().Amount > 6)
                        {
                            slot[i].GetComponent<Slot>().Amount -=6;
                            slot[i].GetComponent<Slot>().UpdateSlot();

                        }
                        slot[i + 1].GetComponent<Slot>().DeleteSlot();
                        slot[i + 1].GetComponent<Slot>().empty = true;


                        slot[i + 1].GetComponent<Slot>().Item = default;
                        slot[i + 1].GetComponent<Slot>().ID = default;
                        slot[i + 1].GetComponent<Slot>().Type = default;
                        slot[i + 1].GetComponent<Slot>().Description = default;
                        slot[i + 1].GetComponent<Slot>().Icon = default;
                        slot[i + 1].GetComponent<Slot>().Amount = default;

                        playableDirector.Play();
                        timer.Run();

                    }
                }
            }

            if (timer.Finished)
            {
                Destroy(gameObject);
                Destroy(text);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {

        text.SetActive(false);
    }
}
