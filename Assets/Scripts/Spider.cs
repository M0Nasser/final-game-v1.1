﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spider : MonoBehaviour
{
    public int damage = 10;
    Animator sAnimation;
    public bool isWalkingSpider;
    // Start is called before the first frame update
    void Start()
    {
        sAnimation = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isWalkingSpider)
        {
            sAnimation.SetBool("IsSpiderWaking", true);
        }
        else
            sAnimation.SetBool("IsSpiderWaking", false);
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(" plyer entred ");
        PlayerMovement plyer = other.GetComponent<PlayerMovement>();
        if (plyer != null)
        {
            Debug.Log(damage);
            plyer.TakeDamage(damage);


        }
    }
}


