﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Build.Content;
using UnityEngine;
using UnityEngine.AI;

public class SpiderFollow : MonoBehaviour
{
    [SerializeField] GameObject palyer;
    [SerializeField] GameObject SpiderObject;
    NavMeshAgent sNavMesh;
   
    
    // Start is called before the first frame update
    void Start()
    {
        sNavMesh = GetComponent<NavMeshAgent>();
        
    }

    // Update is called once per frame
    void Update()
    {

        sNavMesh.SetDestination(palyer.transform.position);
        if(sNavMesh.remainingDistance > 4.0f )
        {

            SpiderObject.GetComponent<Spider>().isWalkingSpider = true;
        }
        else
            SpiderObject.GetComponent<Spider>().isWalkingSpider = false;
            
            
    }
}
