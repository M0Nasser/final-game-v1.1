﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    private bool InventoryEnabled;
    public GameObject inventory;
    public GameObject SlotHolder;

    public int AllSlots;
    public int ActiveSlot;
    public GameObject[] slot;


    void Start()
    {
        //inventory.SetActive(true);
        AllSlots = 2;
        slot = new GameObject[AllSlots];

        for (int i = 0; i < AllSlots; i++)
        {
            slot[i] = SlotHolder.transform.GetChild(i).gameObject;

            if (slot[i].GetComponent<Slot>().Item == null)
                slot[i].GetComponent<Slot>().empty = true;
        }
    }
    // Update is called once per frame
    void Update()
    {
       /* if (Input.GetKeyDown(KeyCode.Tab))
            InventoryEnabled = !InventoryEnabled;

        inventory.SetActive(InventoryEnabled == true);*/
        

    }
    private void OnTriggerStay(Collider other)
    {

        if (other.tag == "Item")
        {
            GameObject PickedItem = other.gameObject;
            Item item = PickedItem.GetComponent<Item>();
            
            if(Input.GetKeyDown(KeyCode.E))
            AddItem(PickedItem, item.ID, item.Type, item.Description, item.Icon);
        }

    }


    void AddItem(GameObject itemObject, int ItemId, string ItemType, string ItemDescription, Sprite ItemIcon)
    {
        for (int i = 0; i < AllSlots; i++)
        {

            if (slot[i].GetComponent<Slot>().ID == ItemId)
            {
                // add item to slot 
                itemObject.GetComponent<Item>().pikedUp = true;

                slot[i].GetComponent<Slot>().Item = itemObject;
                slot[i].GetComponent<Slot>().ID = ItemId;
                slot[i].GetComponent<Slot>().Type = ItemType;
                slot[i].GetComponent<Slot>().Description = ItemDescription;
                slot[i].GetComponent<Slot>().Icon = ItemIcon;
                slot[i].GetComponent<Slot>().Amount +=1;

                itemObject.transform.parent = slot[i].transform;
                itemObject.SetActive(false);

                slot[i].GetComponent<Slot>().UpdateSlot();
                slot[i].GetComponent<Slot>().empty = false;
                return;
            }
        }

            for (int i = 0; i < AllSlots; i++)
        {

            if (slot[i].GetComponent<Slot>().empty)
            {
                // add item to slot 
                itemObject.GetComponent<Item>().pikedUp = true;

                slot[i].GetComponent<Slot>().Item = itemObject;
                slot[i].GetComponent<Slot>().ID = ItemId;
                slot[i].GetComponent<Slot>().Type = ItemType;
                slot[i].GetComponent<Slot>().Description = ItemDescription;
                slot[i].GetComponent<Slot>().Icon = ItemIcon;
                slot[i].GetComponent<Slot>().Amount ++;
                
                itemObject.transform.parent = slot[i].transform;
                itemObject.SetActive(false);

                slot[i].GetComponent<Slot>().UpdateSlot();
                slot[i].GetComponent<Slot>().empty = false;
                return;
            }

        }
    }
}
