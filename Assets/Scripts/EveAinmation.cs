﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EveAinmation : MonoBehaviour
{
    public GameObject pc;
    Animator animator;
    bool walking;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        walking  = pc.GetComponent<PlayerController>().isWalking();
        if (walking)
        {
            animator.SetBool("IsCharcterWalk", true);
        }
        else
            animator.SetBool("IsCharcterWalk", false);
    }
}
