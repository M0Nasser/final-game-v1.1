﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Playsound : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        string s = SceneManager.GetActiveScene().name;
        if ( s == "Level0")
        FindObjectOfType<AudioManager>().Play("Level0Sound");
        else if ( s == "Level1")
        FindObjectOfType<AudioManager>().Play("Level1Sound");
        else if ( s == "Level2")
        FindObjectOfType<AudioManager>().Play("Level2sound");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
