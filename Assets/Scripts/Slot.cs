﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Slot : MonoBehaviour
{
    public GameObject Item;
    public Transform slotIconGO;
    public bool empty;
    public Text TextAmount;

    public int ID;
    public string Type;
    public string Description;
    public Sprite Icon;
    public int Amount;


    private void Start()
    {
        slotIconGO = transform.GetChild(0);
        TextAmount.text = "";
    }
    public void UpdateSlot()
    {
        slotIconGO.GetComponent<Image>().sprite = Icon;
        if (Amount > 1)
            TextAmount.text = Amount.ToString();
        else
            TextAmount.text = "";
    }

    public void DeleteSlot()
    {
        
        slotIconGO.GetComponent<Image>().sprite = default;
        TextAmount.text = "";
    }
}
