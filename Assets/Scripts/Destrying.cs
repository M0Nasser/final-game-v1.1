﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Destrying : MonoBehaviour
{
    [SerializeField] GameObject DestryedVersion;
    [SerializeField] int TimeInSeconds;
    [SerializeField] GameObject Text;
    

    Timer timer;
    string TextPrefix = "Reamins time -> ";
    int TM, TS;
    
    private void Start()
    {
        timer = gameObject.AddComponent<Timer>();
        timer.Duration = TimeInSeconds;
        timer.Run();
        
        Text.SetActive(true);
    }
    void Update()
    {
       
        TM = (int)timer.SecondsLeft / 60;
        TS = (int)timer.SecondsLeft - (TM !=0 ?  TM * 60 -1: 0) ;
        
        //Text.GetComponent<Text>().text = TextPrefix + TM.ToString("00") + " : " + TS.ToString("00");
        Text.GetComponent<Text>().text = "Leave the building quickly";
        if (timer.Finished)
        {
            Text.SetActive(false);
            Instantiate(DestryedVersion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
