﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManger : MonoBehaviour
{

    [SerializeField] GameObject GameOver;
    [SerializeField] GameObject PuaseMenuUi;

    bool GameHasEnded = false;

    // Start is called before the first frame update
    void Start()
    {
        GameOver.SetActive(false);
        PuaseMenuUi.SetActive(false);
    }
    private void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            PuaseMenuUi.SetActive(true);
            Time.timeScale = 0f;
        }*/
    }
    public void EndGame()
    {
        if (GameHasEnded == false)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            GameHasEnded = true;
            Time.timeScale = 0f;
            GameOver.SetActive(true);
        }

    }
    public void HandleRestartButtonOnClickEvent()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1f;
    }

    public void HandleResume()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        PuaseMenuUi.SetActive(false);
        Time.timeScale = 1f;

    }

    public void HandleQuitButtonOnClickEvent()
    {
        Application.Quit();
    }


}
