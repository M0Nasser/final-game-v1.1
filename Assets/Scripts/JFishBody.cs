﻿
using UnityEngine;

public class JFishBody : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        PlayerMovement plyer = other.GetComponent<PlayerMovement>();
        if (plyer != null)
        {
            plyer.TakeDamage(10);

        }
    }
}
