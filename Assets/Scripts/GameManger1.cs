﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManger1 : MonoBehaviour
{
    [SerializeField] GameObject welcome;
    [SerializeField] GameObject Hoppies;
    [SerializeField] GameObject angry;
    [SerializeField] GameObject dreams;
    [SerializeField] GameObject fears;


    [SerializeField] GameObject PuaseMenuUi;

    [SerializeField] Toggle Singing;
    [SerializeField] Toggle Drawing;
    [SerializeField] Toggle nothing;

    [SerializeField] Toggle Stupidity;
    [SerializeField] Toggle Eating;
    [SerializeField] Toggle overlapped;

    [SerializeField] Toggle Travel;
    [SerializeField] Toggle Success;
    [SerializeField] Toggle Blessed;

    [SerializeField] Toggle Spiders;
    [SerializeField] Toggle Heights;
    [SerializeField] Toggle Fly;


    bool BSinging;
    bool BDrawing;
    bool Bnothing;

    bool BStupidity;
    bool BEating;
    bool Boverlapped;

    bool BTravel;
    bool BSuccess;
    bool BBlessed;

    bool BSpiders;
    bool BHeights;
    bool BFly;




    bool GameHasEnded = false;

    // Start is called before the first frame update
    void Start()
    {
        welcome.SetActive(true);
        Hoppies.SetActive(false);
        angry.SetActive(false);
        dreams.SetActive(false);
        fears.SetActive(false);
        PuaseMenuUi.SetActive(false);

    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            PuaseMenuUi.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void HandWlcomeNext()
    {


        welcome.SetActive(false);
        Hoppies.SetActive(true);

    }

    public void HandHoppiesNext()
    {
        BSinging = Singing.isOn;
        BDrawing = Drawing.isOn;
        Bnothing = nothing.isOn;

        Debug.Log(BSinging + "  d  " + BDrawing + "  d  " + Bnothing);
        if (BSinging || BDrawing || Bnothing)
        {
            Hoppies.SetActive(false);
            angry.SetActive(true);
        }
        
    }


    public void HandAngryNext()
    {
        BStupidity = Stupidity.isOn;
        BEating = Eating.isOn;
        Boverlapped = overlapped.isOn;

        Debug.Log(BStupidity + "  d  " + BEating + "  d  " + Boverlapped);
        if(BStupidity || BEating || Boverlapped)
        {

            angry.SetActive(false);
            dreams.SetActive(true);
        }
        

    }


    public void HandDreamNext()
    {
        BTravel = Travel.isOn;
        BSuccess = Success.isOn;
        BBlessed = Blessed.isOn;

        Debug.Log(BTravel + "  d  " + BSuccess + "  d  " + BBlessed);
        if (BTravel || BSuccess || BBlessed)
        {
            dreams.SetActive(false);
            fears.SetActive(true);
        }

    }


    public void HandleHight()
    {
        BSpiders = Spiders.isOn;
        BHeights = Heights.isOn;
        BFly = Fly.isOn;

        Debug.Log(BSpiders + "  d  " + BHeights + "  d  " + BFly);

        if(BSpiders&&!BHeights && !BFly)
            SceneManager.LoadScene("Level2");
        else if(!BSpiders &&( BHeights || BFly ))
            SceneManager.LoadScene("Level1");
        else if (BSpiders && BHeights && BFly)
            SceneManager.LoadScene("Level1");
    }


    public void HandleResume()
    {
        PuaseMenuUi.SetActive(false);
        Time.timeScale = 1f;

    }
    public void HandleQuit()
    {

        Application.Quit();

    }
}
