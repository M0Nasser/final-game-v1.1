﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowTraps : MonoBehaviour
{
    public GameObject TrapObject;
    public Transform DirctionOfThrow;
    public float ThrowForce = 10f;


    public GameObject SlotHolder;


    public int AllSlots;
    public int ActiveSlot;
    public GameObject[] slot;
    private void Start()
    {

        AllSlots = 2;
        slot = new GameObject[AllSlots];

        for (int i = 0; i < AllSlots; i++)
        {
            slot[i] = SlotHolder.transform.GetChild(i).gameObject;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {

            for (int i = 0; i < AllSlots ; i++)
            {
                if (slot[i].GetComponent<Slot>().ID == 0 &&
                    slot[i].GetComponent<Slot>().Amount >= 1)
                {
                    slot[i].GetComponent<Slot>().Amount -= 1;
                    slot[i].GetComponent<Slot>().UpdateSlot();

                    
                    GameObject trap = Instantiate(TrapObject, DirctionOfThrow.position, DirctionOfThrow.rotation) as GameObject;
                    trap.GetComponent<Rigidbody>().AddForce(ThrowForce * DirctionOfThrow.forward, ForceMode.Impulse);

                    if(slot[i].GetComponent<Slot>().Amount == 0 )
                    {
                        slot[i].GetComponent<Slot>().DeleteSlot();
                    }
                }
            }




        }
    }
}
